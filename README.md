# README #

This repository contains a docker image based on ubuntu:latest used by bitbucket pipelines for build  

### What is this repository for? ###

The image contains following tools:

* gcc
* g++
* git
* curl
* cmake

The compilation is done under root and /root/compile is exposed to the host as a volume

