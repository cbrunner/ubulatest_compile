FROM ubuntu:latest

RUN apt-get update && apt-get install -y \
    cmake \
    gcc \
    g++ \
    git \
    curl \
    wget \
    openssl
    
RUN mkdir /root/compile

VOLUME /root/compile